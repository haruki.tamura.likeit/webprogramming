
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserManagementDao;
import model.UserManagement;

/**
 * Servlet implementation class MakeNewServlet
 */
@WebServlet("/MakeNewServlet")
public class MakeNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MakeNewServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserManagement info = (UserManagement) session.getAttribute("userInfo");

		if (info == null) {

			response.sendRedirect("LoginServlet");
			return;

		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/makenewaccount.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
		LocalDateTime now = LocalDateTime.now();

		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password_c = request.getParameter("password_c");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");

		UserManagementDao userDao = new UserManagementDao();
		String user = userDao.findLoginID(login_id);

		//空欄がある場合
		//パスワードの不一致
		//常にあるログイン

		if (user == null) {

			if (login_id.equals("") || password.equals("") || password_c.equals("") || name.equals("")
					|| birth_date.equals("")) {

				request.setAttribute("errMsgg", "未記入欄があります");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/makenewaccount.jsp");
				dispatcher.forward(request, response);
				return;

			} else if (!password.equals(password_c)) {

				request.setAttribute("errMsg", "パスワードが一致しませんでした");

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/makenewaccount.jsp");
				dispatcher.forward(request, response);
				return;

			} else {

				userDao.createUser(login_id, name, birth_date, userDao.password_md(password), dtf.format(now),
						dtf.format(now));

				response.sendRedirect("AccountListServlet");
				return;
				
				
			}

		} else {
			request.setAttribute("errMsgg", "こちらのログインIDはすでに登録されてます");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/makenewaccount.jsp");
			dispatcher.forward(request, response);
			return;

		}

	}
}
