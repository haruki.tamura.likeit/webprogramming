
import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserManagementDao;
import model.UserManagement;

/**
 * Servlet implementation class AccountListServlet
 */
@WebServlet("/AccountListServlet")
public class AccountListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AccountListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		UserManagement info = (UserManagement) session.getAttribute("userInfo");

		if (info == null) {

			response.sendRedirect("LoginServlet");
			return;

		}

		UserManagementDao dao = new UserManagementDao();
		List<UserManagement> empList = dao.findAll();

		request.setAttribute("empList", empList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/accountlist.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String login_id = request.getParameter("login_id");
		String name = request.getParameter("name");
		String birth_date_f = request.getParameter("birth_date_f");
		String birth_date_b = request.getParameter("birth_date_b");

		UserManagementDao userDao = new UserManagementDao();
		List<UserManagement> user = userDao.searchUser1(login_id, name, birth_date_f, birth_date_b);

		if (user == null) {

			request.setAttribute("errMsg", "一覧表示はできませんでした。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/accountlist.jsp");
			dispatcher.forward(request, response);
			return;
		}

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/accountlist.jsp");
		dispatcher.forward(request, response);
		return;

	}

}
