

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.DatatypeConverter;

import dao.UserManagementDao;



public class Jikkou {

	public static void main(String[] args) throws NoSuchAlgorithmException {
		
		   DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");  
		   LocalDateTime now = LocalDateTime.now();  
		
		UserManagementDao userDao = new UserManagementDao();
		userDao.createUser("asdd", "asd", "2021-07-03", "asd", dtf.format(now), dtf.format(now));
		
		//ハッシュを生成したい元の文字列
		String source = "暗号化対象";
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		
		
	
}
}
