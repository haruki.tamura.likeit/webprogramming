
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserManagementDao;
import model.UserManagement;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		UserManagement info = (UserManagement) session.getAttribute("userInfo");

		if (info == null) {

			response.sendRedirect("LoginServlet");
			return;

		}

		String id = request.getParameter("id");

		UserManagementDao userDao = new UserManagementDao();
		UserManagement user = userDao.findById(Integer.parseInt(id));

		request.setAttribute("user", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
		LocalDateTime now = LocalDateTime.now();

		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String password_c = request.getParameter("password_c");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");

		UserManagementDao userDao = new UserManagementDao();
		if (name.equals("")
				|| birth_date.equals("")) {
			request.setAttribute("errMsg", "未記入欄があります");
			
			UserManagement 
			
			UserManagement user = userDao.findById(Integer.parseInt(id));
			request.setAttribute("user", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		else if (password.equals("") && password_c.equals("")) {

			userDao.updateUserNot(name, birth_date, dtf.format(now), login_id);

			response.sendRedirect("AccountListServlet");
			return;

		} else if (password.equals(password_c)) {

			userDao.updateUser(name, birth_date, userDao.password_md(password), dtf.format(now), login_id);

			response.sendRedirect("AccountListServlet");
			return;
		}

		else {

			request.setAttribute("errMsg", "入力された内容は正しくありません。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userupdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

	}

}
