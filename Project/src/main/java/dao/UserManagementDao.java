package dao;




import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.UserManagement;

public class UserManagementDao {

	public List<UserManagement> findAll() {
		Connection conn = null;
		List<UserManagement> empList = new ArrayList<UserManagement>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user where login_id != 'admin' ";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id = rs.getString("login_id");
				String name = rs.getString("name");
				String birth_date = rs.getString("birth_date");
				String password = rs.getString("password");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				UserManagement usermanagement = new UserManagement(id, login_id, name, birth_date, password,
						create_date, update_date);
				empList.add(usermanagement);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return empList;
	}

	public UserManagement findByLoginInfo(String login_id, String password) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new UserManagement(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public UserManagement findById(int id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, id);
			
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int Id = rs.getInt("id");
			String login_id = rs.getString("login_id");
			String name = rs.getString("name");
			String birth_date = rs.getString("birth_date");
			String password = rs.getString("password");
			String create_date = rs.getString("create_date");
			String update_date = rs.getString("update_date");
			return new UserManagement(Id, login_id, name, birth_date, password,
					create_date, update_date);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public void createUser(String login_id, String name, String birth_date, String password, String create_date,
			String update_date) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES (?,?,?,?,?,?)";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			pStmt.setString(2, name);
			pStmt.setString(3, birth_date);
			pStmt.setString(4, password);
			pStmt.setString(5, create_date);
			pStmt.setString(6, update_date);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void updateUser(String name, String birth_date, String password,String update_date,String login_id) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "Update user SET name = ?,password = ?,birth_date= ?,update_date=? WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, password);
			pStmt.setString(3, birth_date);
			pStmt.setString(4, update_date);
			pStmt.setString(5, login_id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
}
	public void updateUserNot(String name, String birth_date, String update_date, String login_id) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "Update user SET name = ?,birth_date= ?,update_date=? WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birth_date);
			pStmt.setString(3, update_date);
			pStmt.setString(4, login_id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
}
	public void deleteUser(String login_id) {
		Connection conn = null;

		try {

			conn = DBManager.getConnection();

			String sql = "DELETE FROM user WHERE login_id = ?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
}
	public UserManagement searchUser(String login_id, String name, String birth_date_f, String birth_date_b) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user Where login_id = ? or name like ? or birth_date between ? and ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			pStmt.setString(2, name);
			pStmt.setString(3, birth_date_f);
			pStmt.setString(4, birth_date_b);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int Id = rs.getInt("id");
			String login_id1 = rs.getString("login_id");
			String name1 = rs.getString("name");
			String birth_date = rs.getString("birth_date");
			String password = rs.getString("password");
			String create_date = rs.getString("create_date");
			String update_date = rs.getString("update_date");
			return new UserManagement(Id, login_id1, name1, birth_date, password,
					create_date, update_date);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
	public List<UserManagement> searchUser1(String login_id, String name, String birth_date_f, String birth_date_b) {
		Connection conn = null;
		List<UserManagement> searchList = new ArrayList<UserManagement>();

		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user Where login_id = ? or name like %?% or birth_date between ? and ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			pStmt.setString(2, name);
			pStmt.setString(3, birth_date_f);
			pStmt.setString(4, birth_date_b);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String login_id1 = rs.getString("login_id");
				String name1 = rs.getString("name");
				String birth_date = rs.getString("birth_date");
				String password = rs.getString("password");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				UserManagement usermanagement = new UserManagement(id, login_id1, name1, birth_date, password,
						create_date, update_date);
				searchList.add(usermanagement);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {

			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return searchList;

	}


	public String password_md(String password) {
	
	String source = password;
	
	Charset charset = StandardCharsets.UTF_8;
	
	String algorithm = "MD5";
	
	String result=""; 
	
	try {
		
	byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	
	result = DatatypeConverter.printHexBinary(bytes);

	
	} catch (NoSuchAlgorithmException e) {
		// TODO 自動生成された catch ブロック
		e.printStackTrace();
	}
	return result;
	}

	public String findLoginID(String login_id) {
		Connection conn = null;
		try {

			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, login_id);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			return loginIdData;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

}
