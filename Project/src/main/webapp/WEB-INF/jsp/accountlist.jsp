<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title></title>
<link href="accountlist.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<style type="text/css">
</style>


</head>

<body>
	<div class="col-9">
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
		<div>
			<header>
				<h1 class="title"></h1>
				<ul class="nav butterfly">
					<li class="nav-item"><a class="nav-link active butterfly1"
						href="LogoutServlet">ログアウト</a></li>
				</ul>
			</header>
		</div>

		<br>



		<h1 class="sample">
			ユーザー一覧 <br>



		</h1>

		<ul class="nav butterfly">
			<li class="nav-item"><a class="nav-link active"
				href="MakeNewServlet">新規登録</a></li>
		</ul>
		<br> <br> <br>
		<form class="form-signin" action="AccountListServlet" method="post">
			<div class="form-group row">
				<label for="inputEmail3" class="col-sm-2 col-form-label">ログインID</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="inputEmail3"
						placeholder="" name="login_id">
				</div>
			</div>
			<div class="form-group row">
				<label for="inputPassword3" class="col-sm-2 col-form-label">ユーザー名</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="inputPassword3"
						placeholder="" name="name">
				</div>
			</div>

			<div class="form-group row">
				<label for="taticsEmail2" class="col-sm-2 col-form-label">生年月日</label>
				<div class="col-4">
					<input type="date" class="form-control" id="taticsEmail2"
						placeholder="年/月/日" name="birth_date_f">
				</div>

				～
				<div class="col-4">
					<input type="date" class="form-control" id="taticsEmail2"
						placeholder="年/月/日" name="birth_date_b">
				</div>
			</div>


			<br> <br>
			<div class="row">
				<div class="col-sm-10"></div>
				<form class="form-inline">

					</label>
			</div>

			<button type="submit" class="btn btn-info buttonton">捜索</button>

		</form>

		<br> <br> <br>
		<div class="table-responsive">
			<table class="table table-bordered">
				<thead class="thead-dark">
					<tr>
					<tr>
						<th scope="col">ログインID</th>
						<th scope="col">ユーザー名</th>
						<th scope="col">生年月日</th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody>

					<c:forEach var="user" items="${empList}">
						<tr>
						
							<td>${user.login_id}</td>
							<td>${user.name}</td>
							<td>${user.birth_date}</td>

							<td>
							<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a> 
							<c:if test="${userInfo.login_id == user.login_id or userInfo.login_id == 'admin'}">
							<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
							</c:if>
							<c:if test="${userInfo.login_id == 'admin'}">
							<a class="btn btn-danger" href="UserDeleteServlet?id=${user.id}">削除</a>
							</c:if>
							</td>
						</tr>
			
					</c:forEach>
					
					<c:forEach var="user1" items="${user}">
						<tr>
						
							<td>${user1.login_id}</td>
							<td>${user1.name}</td>
							<td>${user1.birth_date}</td>

							<td>
							<a class="btn btn-primary" href="UserDetailServlet?id=${user1.id}">詳細</a> 
							<c:if test="${userInfo.login_id == user.login_id or userInfo.login_id == 'admin'}">
							<a class="btn btn-success" href="UserUpdateServlet?id=${user1.id}">更新</a>
							</c:if>
							<c:if test="${userInfo.login_id == 'admin'}">
							<a class="btn btn-danger" href="UserDeleteServlet?id=${user1.id}">削除</a>
							</c:if>
							</td>
							
						</tr>
			
					</c:forEach>

				</tbody>
			</table>
		</div>
</body>

</html>