<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <title></title>
        <link href="makenewaccount.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
            integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style type="text/css">
        </style>
    </head>

    <body>
    	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	
	    	<c:if test="${errMsgg != null}">
		<div class="alert alert-danger" role="alert">${errMsgg}</div>
	</c:if>
	
	    	<c:if test="${errMsggg != null}">
		<div class="alert alert-danger" role="alert">${errMsggg}</div>
	</c:if>

        <div>
            <header class="col-sm-7">
                <h1 class="title">ユーザー名さん</h1>
                <ul class="nav butterfly2">
                    <li class="nav-item"><a class="nav-link active butterfly1" href="#">ログアウト</a></li>
                </ul>
            </header>
            <br>
        </div>
        <div class="col-9">
            <h1 class="sample col-9">
                ユーザ新規登録 <br> <br>




            </h1>

<form class="form-signin" action="MakeNewServlet" method="post">

                <div class="form-group row">
                    <label for="inputEmail3" class="col-sm-3 col-form-label">ログインID</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="inputEmail3" placeholder="" name="login_id">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">パスワード</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" id="inputPassword3" placeholder="" name="password">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">パスワード(確認)</label>
                    <div class="col-sm-5">
                        <input type="password" class="form-control" id="inputPassword3" placeholder=""
                            name="password_c">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">ユーザ名</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="inputPassword3" placeholder="" name = "name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword3" class="col-sm-3 col-form-label">生年月日</label>
                    <div class="col-sm-5">
                        <input type="date" class="form-control" id="inputPassword3" placeholder="" name = "birth_date">
                    </div>
                </div>





                <br>
                <div class="buttonton col-6">
                    <button type="submit" class="btn btn-info">登録</button>
                </div>
        </div>
        
        </form>
        
        <br>
        <div>
            <ul class="nav butterfly">
                <li class="nav-item"><a class="nav-link active" href="AccountListServlet">戻る</a>
                </li>
            </ul>
        </div>
    </body>

    </html>