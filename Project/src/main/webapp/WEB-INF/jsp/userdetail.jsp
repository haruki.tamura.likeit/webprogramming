
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title></title>
<link href="userdetail.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<style type="text/css">
</style>
</head>

<body>


			<div>
				<header class="col-sm-7">
					<h1 class="title">
						<td>${user.name}</td>
					</h1>
					<ul class="nav butterfly2">
						<li class="nav-item"><a class="nav-link active butterfly1"
							href="LoginServlet">ログアウト</a></li>
					</ul>
				</header>
				<br>
			</div>
			<div class="col-9">
				<h1 class="sample col-9">
					ユーザ情報詳細参照 <br> <br>




				</h1>
				<form>
					<div class="form-group row">
						<label for="inputEmail3" class="col-sm-3 col-form-label">ログインID</label>
						<div class="col-sm-5">
							<label for="inputEmail3" class="col-form-label"><td>${user.id}</td></label>
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword3" class="col-sm-3 col-form-label">ユーザ名</label>
						<div class="col-sm-5">
							<label for="inputEmail3" class="col-form-label"><td>${user.login_id}</td></label>
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword3" class="col-sm-3 col-form-label">生年月日</label>
						<div class="col-sm-5">
							<label for="inputEmail3" class="col-form-label"><td>${user.birth_date}</td></label>
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword3" class="col-sm-3 col-form-label">登録日時</label>
						<div class="col-sm-5">
							<label for="inputEmail3" class="col-form-label"><td>${user.create_date}</td></label>
						</div>
					</div>
					<div class="form-group row">
						<label for="inputPassword3" class="col-sm-3 col-form-label">更新日時</label>
						<div class="col-sm-5">
							<label for="inputEmail3" class="col-form-label"><td>${user.update_date}</td></label>
						</div>
					</div>




				</form>


			</div>
			<br>
			<div>
				<ul class="nav butterfly">
					<li class="nav-item"><a class="nav-link active"
						href="AccountListServlet">戻る</a></li>\
				</ul>
			</div>
		</tr>
	
</body>

</html>