<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title></title>
<link href="login.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">
<style type="text/css">
</style>

</head>

<body>
	<c:if test="${errMsg != null}">
		<div class="alert alert-danger" role="alert">${errMsg}</div>
	</c:if>
	<div class="row">
		<div class="col-5">
			<h1 class="sample">
				ログイン画面 <br>

			</h1>

			<form class="form-signin" action="LoginServlet" method="post">
				ログインID
				<div class="form-group mx-sm-3 mb-2">
					<label for="inputPassword2" class="sr-only"></label> <input
						name="login_id" type="text" class="form-control"
						id="inputPassword2" placeholder="ID">
				</div>


				パスワード
				<div class="form-group mx-sm-3 mb-2">
					<label for="inputPassword2" class="sr-only"></label> <input
						name="password" type="password" class="form-control"
						id="inputPassword2" placeholder="•••••••">
				</div>


				<br> <br>


				<div class="row col-7 buttonton">
					<div class="col-10 buttonton">
						<input class="btn btn-primary buttonton" type="submit"
							value="ログイン">
					</div>
				</div>
			</form>
		</div>
	</div>
</body>

</html>